#pragma once

#include <twist/stdlike/mutex.hpp>

namespace solutions {

// Automagically wraps all accesses to guarded object to critical sections
// Look at unit tests for API and usage examples
template <typename T>
class Guarded {
 public:
  using MutexT = twist::stdlike::mutex;

  // https://eli.thegreenplace.net/2014/perfect-forwarding-and-universal-references-in-c/
  template <typename... Args>
  Guarded(Args&&... args) : object_(std::forward<Args>(args)...) {
  }

  class Lock {
   public:
    Lock(T& obj, MutexT& mutex) : obj_ref_(obj), mutex_ref_(mutex) {
      mutex_ref_.lock();
    }

    // https://en.cppreference.com/w/cpp/language/operators
    T* operator->() {
      return &obj_ref_;
    }

    ~Lock() {
      mutex_ref_.unlock();
    }

   private:
    T& obj_ref_;
    MutexT& mutex_ref_;
  };

  Lock operator->() {
    return Lock(object_, mutex_);
  }

 private:
  T object_;
  MutexT mutex_;  // Guards access to object_
};

}  // namespace solutions
