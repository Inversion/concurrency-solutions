#include <asio/read.hpp>
#include <asio/write.hpp>

#include <tinyfibers/net/socket.hpp>

#include <tinyfibers/runtime/scheduler.hpp>
#include <tinyfibers/runtime/parking_lot.hpp>
#include <tinyfibers/runtime/future.hpp>

using wheels::Result;
using wheels::Status;

using wheels::make_result::Fail;
using wheels::make_result::JustStatus;
using wheels::make_result::NotSupported;
using wheels::make_result::Ok;
using wheels::make_result::PropagateError;
using wheels::make_result::ToStatus;

namespace tinyfibers::net {
Result<Socket::ResolveResT> Socket::Resolve(const std::string& host,
                                            uint16_t port) {
  Future<Socket::ResolveResT> endpoints;

  asio::ip::tcp::resolver resolver(GetCurrentContext());
  resolver.async_resolve(
      host, std::to_string(port),
      [&endpoints](const asio::error_code& error, Socket::ResolveResT results) {
        if (error) {
          endpoints.SetError(error);
        } else {
          endpoints.SetValue(results);
        }
      });

  return endpoints.Get();
}

Result<Socket> Socket::Connect(const Socket::EndpointT& endpoint) {
  Future<Socket> socket;

  asio::ip::tcp::socket sock(GetCurrentContext());
  sock.async_connect(endpoint, [&socket, &sock](const asio::error_code& error) {
    if (error) {
      socket.SetError(error);
    } else {
      socket.SetValue(Socket(std::move(sock)));
    }
  });

  return socket.Get();
}

Result<Socket> Socket::ConnectTo(const std::string& host, uint16_t port) {
  auto endpoints = Resolve(host, port);

  if (endpoints.HasError()) {
    return PropagateError(endpoints);
  }

  asio::error_code ec;
  for (const auto& ep : endpoints.ValueUnsafe()) {
    auto retval = Connect(ep);
    if (retval.IsOk()) {
      return retval;
    }
    ec = retval.GetErrorCode();
  }

  return Fail(ec);
}

Result<Socket> Socket::ConnectToLocal(uint16_t port) {
  return ConnectTo("localhost", port);
}

Result<size_t> Socket::ReadSome(MutableBuffer buffer) {
  Future<size_t> result;

  socket_.async_read_some(
      buffer, [&result](const asio::error_code& error, size_t bytes) {
        if (error && error != asio::error::eof) {
          result.SetError(error);
        } else {
          result.SetValue(bytes);
        }
      });

  return result.Get();
}

Result<size_t> Socket::Read(MutableBuffer buffer) {
  Future<size_t> result;

  asio::async_read(socket_, buffer,
                   [&result](const asio::error_code& error, size_t bytes) {
                     if (error && error != asio::error::eof) {
                       result.SetError(error);
                     } else {
                       result.SetValue(bytes);
                     }
                   });

  return result.Get();
}

Status Socket::Write(ConstBuffer buffer) {
  Future<size_t> result;

  asio::async_write(socket_, buffer,
                    [&result](const asio::error_code& error, size_t bytes) {
                      if (error) {
                        result.SetError(error);
                      } else {
                        result.SetValue(bytes);
                      }
                    });

  auto retval = result.Get();

  if (retval.HasError()) {
    PropagateError(retval);
  }

  return Ok();
}

Status Socket::ShutdownWrite() {
  asio::error_code ec;
  socket_.shutdown(socket_.shutdown_send, ec);
  return ToStatus(ec);
}

Socket::Socket(asio::ip::tcp::socket&& impl) : socket_(std::move(impl)) {
}

}  // namespace tinyfibers::net
