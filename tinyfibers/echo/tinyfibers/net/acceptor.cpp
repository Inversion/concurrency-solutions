#include <tinyfibers/net/acceptor.hpp>

#include <tinyfibers/runtime/scheduler.hpp>
#include <tinyfibers/runtime/parking_lot.hpp>
#include <tinyfibers/runtime/future.hpp>

using wheels::Result;
using wheels::Status;

using wheels::make_result::Fail;
using wheels::make_result::JustStatus;
using wheels::make_result::NotSupported;
using wheels::make_result::Ok;
using wheels::make_result::PropagateError;
using wheels::make_result::ToStatus;

namespace tinyfibers::net {

Acceptor::Acceptor() : acceptor_(GetCurrentContext(), asio::ip::tcp::v4()) {
}

Status Acceptor::BindTo(uint16_t port) {
  asio::error_code ec;
  acceptor_.bind({asio::ip::tcp::v4(), port}, ec);
  return ToStatus(ec);
}

Result<uint16_t> Acceptor::BindToAvailablePort() {
  auto status = BindTo(0);
  if (status.HasError()) {
    return PropagateError(status);
  }

  return Ok(GetPort());
}

Status Acceptor::Listen(size_t backlog) {
  asio::error_code ec;
  acceptor_.listen(backlog, ec);
  return ToStatus(ec);
}

Result<Socket> Acceptor::Accept() {
  Future<Socket> socket;
  acceptor_.async_accept(
      [&socket](const asio::error_code& error, asio::ip::tcp::socket sock) {
        if (error) {
          socket.SetError(error);
        } else {
          socket.SetValue(Socket(std::move(sock)));
        }
      });

  return socket.Get();
}

uint16_t Acceptor::GetPort() const {
  asio::error_code ec;
  auto ep = acceptor_.local_endpoint(ec);
  return ep.port();
  /* Why not like this?
  if (ec) {
    return ToStatus(ec);
  }
  return Ok(ep.port());
  */
}

}  // namespace tinyfibers::net
