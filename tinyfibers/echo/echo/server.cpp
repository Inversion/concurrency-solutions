#include <string>

#include <echo/server.hpp>

#include <tinyfibers/api.hpp>
#include <tinyfibers/net/acceptor.hpp>
#include <tinyfibers/net/socket.hpp>

using tinyfibers::Spawn;
using tinyfibers::net::Acceptor;
using tinyfibers::net::Socket;

namespace echo {

void HandleClient(Socket socket) {
  static const size_t kBuffSize = 1024;
  try {
    std::string buff(kBuffSize, 0);
    for (;;) {
      auto read_res = socket.ReadSome({buff.data(), buff.size()});
      auto bytes = read_res.ValueOrThrow();
      if (bytes == 0) {
        socket.ShutdownWrite().ThrowIfError();
        break;
      }
      socket.Write({buff.data(), bytes}).Ignore();
    }
  } catch (...) {
    std::cout << "Handler failed" << std::endl;
  }
}

void ServeForever(uint16_t port) {
  try {
    Acceptor acceptor;
    acceptor.BindTo(port).ThrowIfError();
    acceptor.Listen().ThrowIfError();
    for (;;) {
      auto sock = acceptor.Accept().ValueOrThrow();
      auto handler = Spawn([sock{std::move(sock)}]() mutable {
        HandleClient(std::move(sock));
      });
      handler.Detach();
    }
  } catch (...) {
    std::cout << "Server failed" << std::endl;
  }
}
}  // namespace echo
