#pragma once

#include <cinttypes>

namespace mtf::fibers {
namespace support {
inline uint64_t FastRand(void) {
  static thread_local uint64_t x = 123456789, y = 362436069, z = 521288629;
  x ^= x << 16;
  x ^= x >> 5;
  x ^= x << 1;

  uint64_t t = x;
  x = y;
  y = z;
  z = t ^ x ^ y;

  return z;
}

}  // namespace support
}  // namespace mtf::fibers
