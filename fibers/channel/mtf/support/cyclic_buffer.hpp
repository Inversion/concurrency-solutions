#pragma once

#include <wheels/support/assert.hpp>

#include <vector>

namespace mtf::fibers {
namespace support {

template <typename T>
class CyclicBuffer {
 public:
  CyclicBuffer(size_t capacity) : capacity_(capacity) {
    WHEELS_VERIFY(capacity > 0, "Capacity == 0");
    buffer_.reserve(capacity);
  }

  void Put(T value) {
    WHEELS_VERIFY(size_ < capacity_, "Put on size_ == capacity_");
    if (buffer_.size() < capacity_) {
      buffer_.emplace_back(std::move(value));
    } else {
      buffer_.at(tail_) = std::move(value);
    }
    tail_ = (tail_ + 1) % capacity_;
    ++size_;
  }

  T& Front() {
    return buffer_.at(head_);
  }

  void PopFront() {
    head_ = (head_ + 1) % capacity_;
    --size_;
  }

  bool IsEmpty() const {
    return size_ == 0;
  }

  bool IsFull() const {
    return size_ == capacity_;
  }

 private:
  const size_t capacity_;

  std::vector<T> buffer_;
  size_t head_{0};
  size_t tail_{0};
  size_t size_{0};
};
}  // namespace support
}  // namespace mtf::fibers
