#pragma once

#include <wheels/support/assert.hpp>

#include <iostream>

namespace mtf::fibers {
namespace support {
template <typename LockT>
class UniqueLock {
 public:
  UniqueLock(LockT& lockable) : lockable_(lockable) {
    lockable_.Lock();
  }

  void Lock() {
    lockable_.Lock();
    WHEELS_ASSERT(!owns_, "Already locked");
    owns_ = true;
  }

  void Unlock() {
    WHEELS_ASSERT(owns_, "Not locked");
    owns_ = false;
    lockable_.Unlock();
  }

  ~UniqueLock() {
    if (owns_) {
      lockable_.Unlock();
    }
  }

 private:
  LockT& lockable_;
  bool owns_{true};
};
}  // namespace support
}  // namespace mtf::fibers
