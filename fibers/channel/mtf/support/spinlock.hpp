#pragma once

#include <twist/stdlike/atomic.hpp>
#include <wheels/support/cpu.hpp>

namespace mtf::fibers {
namespace support {
class TASSpinLock {
 public:
  void Lock() {
    while (locked_.exchange(1) != 0) {
      while (locked_.load() != 0) {
        wheels::SpinLockPause();
      }
    }
  }

  void Unlock() {
    locked_.store(0);
  }

  inline void lock() {  // NOLINT
    Lock();
  }

  inline void unlock() {  // NOLINT
    Unlock();
  }

 private:
  twist::stdlike::atomic<int> locked_{0};
};
}  // namespace support
}  // namespace mtf::fibers
