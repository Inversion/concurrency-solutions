#include <mtf/coroutine/impl.hpp>

#include <exception>

namespace mtf::coroutine::impl {

static thread_local Coroutine* current;
static thread_local std::exception_ptr exception{nullptr};

Coroutine::Coroutine(Routine routine, context::StackView stack)
    : routine_(std::move(routine)) {
  this_ex_con_.Setup(stack, Coroutine::Trampoline);
}

void Coroutine::Resume() {
  auto* saved_current = current;
  current = this;
  caller_ex_con_.SwitchTo(this_ex_con_);
  current = saved_current;

  if (exception) {
    auto rethrow = exception;
    exception = nullptr;
    std::rethrow_exception(rethrow);
  }
}

void Coroutine::Suspend() {
  current->SuspendImpl();
}

void Coroutine::SuspendImpl() {
  this_ex_con_.SwitchTo(caller_ex_con_);
}

bool Coroutine::IsCompleted() const {
  return is_completed_;
}

void Coroutine::Trampoline() {
  current->this_ex_con_.AfterStart();

  try {
    current->routine_();
  } catch (...) {
    exception = std::current_exception();
  }

  auto cur_coroutine = current;
  current = nullptr;

  cur_coroutine->is_completed_ = true;
  cur_coroutine->this_ex_con_.SwitchTo(cur_coroutine->caller_ex_con_);

  std::abort();
}

}  // namespace mtf::coroutine::impl
