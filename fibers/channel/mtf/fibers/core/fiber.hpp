#pragma once

#include <mtf/fibers/core/api.hpp>

#include <mtf/coroutine/impl.hpp>
#include <mtf/fibers/core/stacks.hpp>
#include <mtf/fibers/core/handle.hpp>
#include <mtf/fibers/core/suspend.hpp>

#include <context/stack.hpp>

#include <memory>

namespace mtf::fibers {

class Fiber {
  using Stack = context::Stack;
  using Coroutine = coroutine::impl::Coroutine;

 public:
  Fiber(Stack stack, Routine routine, Scheduler& scheduler);
  ~Fiber();

  static Fiber& AccessCurrent();

  // ~ System calls

  static void Spawn(Routine routine, Scheduler& scheduler);

  void Yield();
  void Suspend(IAwaiter*);
  void Resume();

 private:
  void Stop();
  void Step();
  void Schedule();
  void Reschedule();
  void Destroy();
  void Await();

  enum class FiberState { Runnable, Suspended, Terminated };

 private:
  static SimpleStackPool stacks;

  Stack stack_;
  Coroutine coroutine_;
  Scheduler& scheduler_;

  FiberState state_;
  IAwaiter* awaiter_{nullptr};
};

}  // namespace mtf::fibers
