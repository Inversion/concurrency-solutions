#pragma once

#include <mtf/fibers/core/handle.hpp>

#include <memory>

namespace mtf::fibers {

class IAwaiter {
 public:
  virtual void Suspend(FiberHandle) = 0;
};

void Suspend(IAwaiter*);

}  // namespace mtf::fibers
