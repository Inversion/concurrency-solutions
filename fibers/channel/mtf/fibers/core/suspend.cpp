#include <mtf/fibers/core/suspend.hpp>

#include <mtf/fibers/core/fiber.hpp>
#include <mtf/fibers/core/handle.hpp>

namespace mtf::fibers {

void Suspend(IAwaiter* awaiter) {
  Fiber::AccessCurrent().Suspend(awaiter);
}

}  // namespace mtf::fibers
