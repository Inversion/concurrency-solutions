#pragma once

#include <mtf/fibers/core/suspend.hpp>
#include <mtf/fibers/sync/channel.hpp>
#include <mtf/fibers/sync/channel_impl.hpp>

#include <mtf/support/spinlock.hpp>
#include <mtf/support/fastrand.hpp>
#include <mtf/support/unique_lock.hpp>

#include <optional>
#include <variant>

// https://golang.org/src/runtime/chan.go
// https://golang.org/src/runtime/select.go

namespace mtf::fibers {

namespace detail {

template <size_t I, typename VariantT, typename LockT>
class SelectorReceiver
    : public IReceiver<std::variant_alternative_t<I, VariantT>> {
  using ValueT = std::variant_alternative_t<I, VariantT>;
  using OptionT = std::optional<VariantT>;
  using GuardT = support::UniqueLock<LockT>;

 public:
  SelectorReceiver(OptionT& option, LockT& lock, FiberHandle& handle)
      : option_(option), lock_(lock), handle_(handle) {
  }

  bool TryReceive(ValueT& value) override {
    GuardT lock(lock_);

    if (option_.has_value()) {
      return false;
    }

    option_.emplace(std::in_place_index_t<I>{}, std::move(value));

    if (handle_.IsValid()) {
      handle_.Resume();
    }

    return true;
  }

 private:
  OptionT& option_;
  LockT& lock_;
  FiberHandle& handle_;
};

template <typename X, typename Y>
class Selector : public IAwaiter {
  using LockT = support::TASSpinLock;
  using SelectedValueT = std::variant<X, Y>;
  template <size_t I>
  using ReceiverT = SelectorReceiver<I, SelectedValueT, LockT>;
  using OptionT = std::optional<SelectedValueT>;

 public:
  Selector(Channel<X>& xs, Channel<Y>& ys)
      : xchan_(xs), ychan_(ys), handle_(FiberHandle::Invalid()) {
  }

  void Suspend(FiberHandle handle) override {
    handle_ = std::move(handle);
    mutex_.Unlock();
  }

  SelectedValueT Select() {
    ReceiverT<0> xreceiver(option_, mutex_, handle_);
    ReceiverT<1> yreceiver(option_, mutex_, handle_);

    if ((support::FastRand() & 1) == 0) {
      xchan_.impl_->Subscribe(xreceiver);
      ychan_.impl_->Subscribe(yreceiver);
    } else {
      ychan_.impl_->Subscribe(yreceiver);
      xchan_.impl_->Subscribe(xreceiver);
    }

    // Here usage of guard would be
    // problematic because
    // awaiter is this
    mutex_.Lock();
    if (!option_.has_value()) {
      mtf::fibers::Suspend(this);
      mutex_.Lock();
    }

    WHEELS_ASSERT(option_.has_value(), "No value");

    auto retval = std::move(option_.value());

    mutex_.Unlock();

    xchan_.impl_->Unsubscribe(xreceiver);
    ychan_.impl_->Unsubscribe(yreceiver);

    return retval;
  }

 private:
  Channel<X>& xchan_;
  Channel<Y>& ychan_;

  LockT mutex_;
  FiberHandle handle_;

  OptionT option_;
};

}  // namespace detail

/*
 * Usage:
 * Channel<X> xs;
 * Channel<Y> ys;
 * ...
 * // value - std::variant<X, Y>
 * auto value = Select(xs, ys);
 * switch (value.index()) {
 *   case 0:
 *     // Handle std::get<0>(value);
 *     break;
 *   case 1:
 *     // Handle std::get<1>(value);
 *     break;
 * }
 */

template <typename X, typename Y>
auto Select(Channel<X>& xs, Channel<Y>& ys) {
  detail::Selector<X, Y> selector(xs, ys);
  return selector.Select();
}

}  // namespace mtf::fibers
