#include <mtf/fibers/sync/condvar.hpp>

namespace mtf::fibers {

void CondVar::Wait(Lock& lock) {
  auto position = wait_queue_.load();
  lock.unlock();
  wait_queue_.ParkIfEqual(position);
  lock.lock();
}

void CondVar::NotifyOne() {
  wait_queue_.fetch_add(1);
  wait_queue_.WakeOne();
}

void CondVar::NotifyAll() {
  wait_queue_.fetch_add(1);
  wait_queue_.WakeAll();
}

}  // namespace mtf::fibers
