#pragma once

#include <mtf/fibers/core/api.hpp>
#include <mtf/fibers/core/handle.hpp>
#include <mtf/fibers/core/suspend.hpp>

#include <mtf/support/spinlock.hpp>
#include <mtf/support/unique_lock.hpp>

#include <twist/stdlike/atomic.hpp>
#include <wheels/support/intrusive_list.hpp>

namespace mtf::fibers {

// https://eli.thegreenplace.net/2018/basics-of-futexes/

template <typename Lock>
class FutexAwaiter : public IAwaiter {
 public:
  using QueueT = wheels::IntrusiveList<FiberHandle>;

  FutexAwaiter(QueueT& queue, Lock& lock)
      : handle_(FiberHandle::Invalid()), queue_(queue), lock_(lock) {
  }

  void Suspend(FiberHandle handle) override {
    handle_ = handle;
    queue_.PushBack(&handle_);
    lock_.Unlock();
  }

 private:
  FiberHandle handle_;
  QueueT& queue_;
  Lock& lock_;
};

template <typename T>
class FutexLike : public twist::stdlike::atomic<T> {
 public:
  explicit FutexLike(T initial_value)
      : twist::stdlike::atomic<T>(initial_value) {
  }

  ~FutexLike() {
    WHEELS_ASSERT(wait_queue_.IsEmpty(), "Wait queue not empty");
  }

  // Park current fiber if value of atomic is equal to `old`
  void ParkIfEqual(T old) {
    support::UniqueLock lock(mutex_);
    if (twist::stdlike::atomic<T>::load() == old) {
      FutexAwaiter awaiter(wait_queue_, lock);
      Suspend(&awaiter);
    }
  }

  void WakeOne() {
    support::UniqueLock lock(mutex_);
    if (!wait_queue_.IsEmpty()) {
      wait_queue_.PopFront()->Resume();
    }
  }

  void WakeAll() {
    // Here mutex really looks better
    // than guard
    mutex_.Lock();
    auto queue = std::move(wait_queue_);
    mutex_.Unlock();

    for (auto& handle : queue) {
      handle.Resume();
    }
  }

 private:
  support::TASSpinLock mutex_;
  wheels::IntrusiveList<FiberHandle> wait_queue_;
};
}  // namespace mtf::fibers
