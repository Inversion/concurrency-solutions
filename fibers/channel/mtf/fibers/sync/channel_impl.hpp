#pragma once

#include <mtf/fibers/core/suspend.hpp>
#include <mtf/support/spinlock.hpp>
#include <mtf/support/cyclic_buffer.hpp>
#include <mtf/support/unique_lock.hpp>

#include <wheels/support/intrusive_list.hpp>
#include <wheels/support/assert.hpp>

#include <vector>
#include <optional>

/*
 * https://www.youtube.com/watch?v=cdpQMDgQP8Y
 * https://github.com/golang/go/issues/8899
 */

namespace mtf::fibers {

namespace detail {

template <typename T>
class IReceiver : public wheels::IntrusiveListNode<IReceiver<T>> {
 public:
  virtual bool TryReceive(T& value) = 0;
};

template <typename T, typename LockT>
class ChannelSender : public wheels::IntrusiveListNode<ChannelSender<T, LockT>>,
                      public IAwaiter {
 public:
  ChannelSender(T& value, LockT& lock)
      : value_(value), lock_(lock), handle_(FiberHandle::Invalid()) {
  }

  void Suspend(FiberHandle handle) override {
    handle_ = std::move(handle);
    lock_.Unlock();
  }

  T&& Take() {
    return std::move(value_);
  }

  void Resume() {
    handle_.Resume();
  }

 private:
  T& value_;
  LockT& lock_;

  FiberHandle handle_;
};

template <typename T, typename LockT>
class ChannelReceiver : public IReceiver<T>, public IAwaiter {
 public:
  ChannelReceiver(std::optional<T>& value, LockT& lock)
      : value_(value), lock_(lock), handle_(FiberHandle::Invalid()) {
  }

  void Suspend(FiberHandle handle) override {
    handle_ = std::move(handle);
    lock_.Unlock();
  }

  bool TryReceive(T& value) override {
    value_.emplace(std::move(value));
    // If we are sleeping now
    if (handle_.IsValid()) {
      handle_.Resume();
    }
    return true;
  }

 private:
  std::optional<T>& value_;
  LockT& lock_;

  FiberHandle handle_;
};

template <typename T>
class ChannelImpl {
  using LockT = support::TASSpinLock;
  using GuardT = support::UniqueLock<LockT>;
  using SenderT = ChannelSender<T, GuardT>;
  using ReceiverT = ChannelReceiver<T, GuardT>;
  using IReceiverT = IReceiver<T>;

 public:
  ChannelImpl(size_t capacity) : buffer_(capacity) {
  }

  ~ChannelImpl() {
    WHEELS_ASSERT(receivers_.IsEmpty(), "Has pending Receive");
    WHEELS_ASSERT(senders_.IsEmpty(), "Has pending Send");
  }

  void Send(T value) {
    GuardT lock(mutex_);

    if (!TryGiveToReceiver(value) && !TryPutToBuffer(value)) {
      SenderT sender(value, lock);
      senders_.PushBack(&sender);
      Suspend(&sender);
    }
  }

  T Receive() {
    GuardT lock(mutex_);

    std::optional<T> value;
    ReceiverT receiver(value, lock);

    FulfillOrSubscribe(receiver);

    // Was not fullfilled
    if (!value.has_value()) {
      Suspend(&receiver);
    }

    WHEELS_ASSERT(value.has_value(), "No value");

    return std::move(value.value());
  }

  void Subscribe(IReceiverT& receiver) {
    GuardT lock(mutex_);
    FulfillOrSubscribe(receiver);
  }

  void Unsubscribe(IReceiverT& receiver) {
    GuardT lock(mutex_);
    // User responsible
    // for passing
    // receiver of this channel
    receiver.Unlink();
  }

 private:
  inline bool TryGiveToReceiver(T& value) {
    while (!receivers_.IsEmpty()) {
      auto receiver = receivers_.PopFront();
      if (receiver->TryReceive(value)) {
        return true;
      }
    }

    return false;
  }

  inline bool TryPutToBuffer(T& value) {
    if (!buffer_.IsFull()) {
      buffer_.Put(std::move(value));
      return true;
    }

    return false;
  }

  inline void FulfillOrSubscribe(IReceiverT& receiver) {
    if (buffer_.IsEmpty()) {
      receivers_.PushBack(&receiver);
    } else if (receiver.TryReceive(buffer_.Front())) {
      buffer_.PopFront();

      // Do work for sender
      if (!senders_.IsEmpty()) {
        auto sender = senders_.PopFront();
        buffer_.Put(sender->Take());
        sender->Resume();
      }
    }
  }

 private:
  LockT mutex_;
  support::CyclicBuffer<T> buffer_;
  wheels::IntrusiveList<SenderT> senders_;
  wheels::IntrusiveList<IReceiverT> receivers_;
};

}  // namespace detail

}  // namespace mtf::fibers
