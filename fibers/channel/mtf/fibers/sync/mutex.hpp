#pragma once

#include <mtf/fibers/sync/futex.hpp>

namespace mtf::fibers {

class Mutex {
 public:
  void Lock() {
    waiting_.fetch_add(1);
    while (acquired_.exchange(1) != 0u) {
      acquired_.ParkIfEqual(1);
    }
  }

  void Unlock() {
    acquired_.store(0);
    if (waiting_.fetch_sub(1) != 1u) {
      acquired_.WakeOne();
    }
  }

  // BasicLockable concept

  void lock() {  // NOLINT
    Lock();
  }

  void unlock() {  // NOLINT
    Unlock();
  }

 private:
  FutexLike<uint32_t> acquired_{0};
  twist::stdlike::atomic<uint32_t> waiting_{0};
};

}  // namespace mtf::fibers
