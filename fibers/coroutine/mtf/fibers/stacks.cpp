#include <mtf/fibers/stacks.hpp>

namespace mtf::fibers {

using context::Stack;

Stack SimpleStackPool::AllocateStack() {
  mutex_.Lock();
  if (pool_.empty()) {
    mutex_.Unlock();

    return Stack::AllocatePages(kStackPages);
  } else {
    Stack retval{std::move(pool_.top())};
    pool_.pop();
    mutex_.Unlock();

    return retval;
  }
}

void SimpleStackPool::ReleaseStack(Stack stack) {
  mutex_.Lock();
  pool_.emplace(std::move(stack));
  mutex_.Unlock();
}
}  // namespace mtf::fibers
