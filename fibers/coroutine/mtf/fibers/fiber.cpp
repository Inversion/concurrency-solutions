#include <mtf/fibers/api.hpp>

#include <mtf/coroutine/impl.hpp>
#include <mtf/fibers/stacks.hpp>

#include <memory>

namespace mtf::fibers {

using coroutine::impl::Coroutine;
using tp::StaticThreadPool;

////////////////////////////////////////////////////////////////////////////////

class Fiber : public std::enable_shared_from_this<Fiber> {
  using Stack = context::Stack;
  using Task = mtf::tp::Task;

 public:
  Fiber(Routine routine, StaticThreadPool& scheduler)
      : stack_(stacks.AllocateStack()),
        coroutine_(std::move(routine), stack_.View()),
        scheduler_(scheduler) {
  }

  void Resume() {
    coroutine_.Resume();

    if (coroutine_.IsCompleted()) {
      stacks.ReleaseStack(std::move(stack_));
    } else {
      auto fiber = shared_from_this();
      scheduler_.Submit([fiber] {
        fiber->Resume();
      });
    }
  }

 private:
  static SimpleStackPool stacks;

  Stack stack_;
  Coroutine coroutine_;
  StaticThreadPool& scheduler_;
};

SimpleStackPool Fiber::stacks;

////////////////////////////////////////////////////////////////////////////////

void Spawn(Routine routine, StaticThreadPool& scheduler) {
  scheduler.Submit([routine{std::move(routine)}, &scheduler] {
    auto fiber = std::make_shared<Fiber>(std::move(routine), scheduler);
    fiber->Resume();
  });
}

void Spawn(Routine routine) {
  Spawn(std::move(routine), *mtf::tp::Current());
}

void Yield() {
  Coroutine::Suspend();
}

}  // namespace mtf::fibers
