#pragma once

#include <context/stack.hpp>
#include <mtf/fibers/spinlock.hpp>

#include <stack>

namespace mtf::fibers {

class SimpleStackPool {
 public:
  context::Stack AllocateStack();
  void ReleaseStack(context::Stack stack);

 private:
  static const size_t kStackPages = 8;

  std::stack<context::Stack> pool_;  // GUARDED BY mutex
  TASSpinLock mutex_;
};

}  // namespace mtf::fibers
