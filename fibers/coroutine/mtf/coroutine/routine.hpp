#pragma once

#include <functional>
#include <wheels/support/function.hpp>

namespace mtf::coroutine {

// using Routine = wheels::UniqueFunction<void()>;
using Routine = std::function<void()>;

}  // namespace mtf::coroutine
