#pragma once

#include <functional>
#include <wheels/support/function.hpp>

namespace mtf::tp {

// using Task = wheels::UniqueFunction<void()>;
using Task = std::function<void()>;

}  // namespace mtf::tp
