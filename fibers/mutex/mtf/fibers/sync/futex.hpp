#pragma once

#include <mtf/fibers/core/api.hpp>
#include <mtf/fibers/core/handle.hpp>
#include <mtf/fibers/core/suspend.hpp>

#include <twist/stdlike/atomic.hpp>
#include <twist/stdlike/mutex.hpp>

#include <wheels/support/intrusive_list.hpp>

#include <mutex>

namespace mtf::fibers {

// https://eli.thegreenplace.net/2018/basics-of-futexes/

template <typename T>
class FutexLike : public twist::stdlike::atomic<T> {
  using Base = twist::stdlike::atomic<T>;

 public:
  explicit FutexLike(T initial_value)
      : twist::stdlike::atomic<T>(initial_value) {
  }

  ~FutexLike() {
    WHEELS_ASSERT(wait_queue_.IsEmpty(), "Wait queue not empty");
  }

  // Park current fiber if value of atomic is equal to `old`
  void ParkIfEqual(T old) {
    auto handle = FiberHandle::Invalid();
    {
      std::unique_lock lock(mutex_);
      if (Base::load() != old) {
        return;
      }
      handle = Suspend();
      wait_queue_.PushFront(&handle);
    }
    Yield();
  }

  void WakeOne() {
    std::unique_lock lock(mutex_);
    if (!wait_queue_.IsEmpty()) {
      wait_queue_.PopFront()->Resume();
    }
  }

  void WakeAll() {
    std::unique_lock lock(mutex_);
    for (auto handle : wait_queue_) {
      handle.Resume();
    }
    wait_queue_.UnlinkAll();
  }

 private:
  twist::stdlike::mutex mutex_;
  wheels::IntrusiveList<FiberHandle> wait_queue_;
};
}  // namespace mtf::fibers
