#pragma once

#include <mtf/fibers/sync/futex.hpp>

namespace mtf::fibers {

class Mutex {
 public:
  void Lock() {
    waiting_.fetch_add(1);
    while (acquired_.exchange(true)) {
      acquired_.ParkIfEqual(true);
    }
  }

  void Unlock() {
    acquired_.store(false);
    if (waiting_.fetch_sub(1) != 1u) {
      acquired_.WakeOne();
    }
  }

  // BasicLockable concept

  void lock() {  // NOLINT
    Lock();
  }

  void unlock() {  // NOLINT
    Unlock();
  }

 private:
  FutexLike<bool> acquired_{false};
  twist::stdlike::atomic<uint32_t> waiting_{0};
};

}  // namespace mtf::fibers
