#include <mtf/fibers/core/fiber.hpp>

#include <mtf/fibers/core/stacks.hpp>
#include <mtf/fibers/core/handle.hpp>
#include <mtf/coroutine/impl.hpp>

#include <wheels/support/assert.hpp>
#include <wheels/support/exception.hpp>

namespace mtf::fibers {

SimpleStackPool Fiber::stacks;

static thread_local Fiber* current = nullptr;

Fiber& Fiber::AccessCurrent() {
  WHEELS_ASSERT(current, "Not in fiber context");
  return *current;
}

Fiber::Fiber(Stack stack, Routine routine, Scheduler& scheduler)
    : stack_(std::move(stack)),
      coroutine_(std::move(routine), stack_.View()),
      scheduler_(scheduler),
      state_(FiberState::Runnable) {
}

Fiber::~Fiber() {
}

void Fiber::Spawn(Routine routine, Scheduler& scheduler) {
  Stack stack = stacks.AllocateStack();
  Fiber* fiber = new Fiber(std::move(stack), std::move(routine), scheduler);
  fiber->Resume();
}

// System calls

void Fiber::Yield() {
  Stop();
}

FiberHandle Fiber::Suspend() {
  auto state = state_.exchange(FiberState::Suspended);
  WHEELS_ASSERT(state == FiberState::Runnable, "Unexpected fiber state");
  return FiberHandle(this);
}

void Fiber::Resume() {
  auto state = state_.exchange(FiberState::Runnable);
  switch (state) {
    case FiberState::Runnable:
      Schedule();
      break;
    case FiberState::Suspended:
      // Will be rescheduled by suspending thread
      break;
    default:
      WHEELS_PANIC("Unexpected Fiber state");
      break;
  }
}

// Scheduler ops

void Fiber::Schedule() {
  scheduler_.Submit([this] {
    Step();
    Reschedule();
  });
}

void Fiber::Reschedule() {
  auto state = state_.exchange(FiberState::Runnable);
  switch (state) {
    case FiberState::Runnable:
      Schedule();
      break;
    case FiberState::Suspended:
      // No-op
      break;
    case FiberState::Terminated:
      Destroy();
      break;
    default:
      WHEELS_PANIC("Unexpected Fiber state");
      break;
  }
}

void Fiber::Await() {
  // Was not comprehended
}

void Fiber::Destroy() {
  stacks.ReleaseStack(std::move(stack_));
  delete this;
}

void Fiber::Step() {
  current = this;
  coroutine_.Resume();
  if (coroutine_.IsCompleted()) {
    auto state = state_.exchange(FiberState::Terminated);
    WHEELS_ASSERT(state == FiberState::Runnable, "Unexpected Fiber state");
  }
  current = nullptr;
}

void Fiber::Stop() {
  Coroutine::Suspend();
}

}  // namespace mtf::fibers
