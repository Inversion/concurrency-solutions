#include <mtf/fibers/core/suspend.hpp>

#include <mtf/fibers/core/fiber.hpp>
#include <mtf/fibers/core/handle.hpp>

namespace mtf::fibers {

FiberHandle Suspend() {
  return Fiber::AccessCurrent().Suspend();
}

}  // namespace mtf::fibers
