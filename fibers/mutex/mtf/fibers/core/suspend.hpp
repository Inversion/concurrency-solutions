#pragma once

#include <mtf/fibers/core/handle.hpp>

namespace mtf::fibers {

FiberHandle Suspend();

}  // namespace mtf::fibers
