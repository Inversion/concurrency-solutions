#pragma once

#include <twist/stdlike/atomic.hpp>

#include <cstdint>

namespace solutions {

class ConditionVariable {
 public:
  // Mutex - BasicLockable
  // https://en.cppreference.com/w/cpp/named_req/BasicLockable
  template <class Mutex>
  void Wait(Mutex& mutex) {
    auto position = wait_queue_.load();
    mutex.unlock();
    wait_queue_.wait(position);
    mutex.lock();
  }

  void NotifyOne() {
    wait_queue_.fetch_add(1);
    wait_queue_.notify_one();
  }

  void NotifyAll() {
    wait_queue_.fetch_add(1);
    wait_queue_.notify_all();
  }

 private:
  twist::stdlike::atomic<uint32_t> wait_queue_{0};
};

}  // namespace solutions
