#include <tp/static_thread_pool.hpp>

#include <tp/helpers.hpp>

#include <twist/util/thread_local.hpp>

namespace tp {

////////////////////////////////////////////////////////////////////////////////

static twist::util::ThreadLocal<StaticThreadPool*> pool{nullptr};

////////////////////////////////////////////////////////////////////////////////

StaticThreadPool::StaticThreadPool(size_t workers) {
  auto worker_routine = [current = this, &tasks = tasks_, &joiner = joiner_]() {
    *pool = current;
    while (auto task = tasks.Take()) {
      tp::ExecuteHere(*task);
      joiner.Decrement();
    }
  };

  for (size_t i = 0; i < workers; ++i) {
    workers_.emplace_back(worker_routine);
  }
}

StaticThreadPool::~StaticThreadPool() {
  assert(is_properly_closed_);
}

bool StaticThreadPool::IsSubmitAllowed() {
  return !joiner_.IsWaiting() || tp::Current() == this;
}

void StaticThreadPool::Submit(Task task) {
  if (IsSubmitAllowed()) {
    joiner_.IncrementOptimistic([&tasks = tasks_, &task]() {
      return tasks.Put(std::move(task));
    });
  }
}

void StaticThreadPool::CloseProperly() {
  for (size_t i = 0; i < workers_.size(); ++i) {
    workers_.at(i).join();
  }
  is_properly_closed_ = true;
}

void StaticThreadPool::Join() {
  joiner_.Wait();
  tasks_.Close();
  CloseProperly();
}

void StaticThreadPool::Shutdown() {
  tasks_.Cancel();
  CloseProperly();
}

StaticThreadPool* StaticThreadPool::Current() {
  return *pool;
}

}  // namespace tp
