#pragma once

#include <twist/stdlike/atomic.hpp>
#include <wheels/support/function.hpp>

namespace tp {
class AwaitableCounter {
 public:
  void IncrementOptimistic(wheels::UniqueFunction<bool()>);

  void Decrement();

  void Wait();

  bool IsWaiting();

 private:
  twist::stdlike::atomic<uint32_t> counter_{0};
  twist::stdlike::atomic<uint32_t> is_waiting_{0};
};
}  // namespace tp
