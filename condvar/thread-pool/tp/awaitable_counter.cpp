#include <tp/awaitable_counter.hpp>

namespace tp {
void AwaitableCounter::IncrementOptimistic(
    wheels::UniqueFunction<bool()> predicate) {
  counter_.fetch_add(1);
  if (!predicate()) {
    counter_.fetch_sub(1);
  }
}

void AwaitableCounter::Decrement() {
  if (counter_.fetch_sub(1) == 1 && (is_waiting_.load() != 0u)) {
    counter_.notify_all();
  }
}

void AwaitableCounter::Wait() {
  is_waiting_.store(1);
  while (auto number = counter_.load()) {
    counter_.wait(number);
  }
}

bool AwaitableCounter::IsWaiting() {
  return is_waiting_.load() != 0u;
}
}  // namespace tp
