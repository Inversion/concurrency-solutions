#pragma once

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>

#include <optional>
#include <deque>

namespace tp {

// Unbounded blocking multi-producers/multi-consumers queue

template <typename T>
class UnboundedBlockingQueue {
 public:
  bool Put(T value) {
    std::unique_lock lock(mutex_);
    if (is_closed_) {
      return false;
    }
    buffer_.emplace_back(std::move(value));
    takeable_.notify_one();
    return true;
  }

  std::optional<T> Take() {
    std::unique_lock lock(mutex_);
    while (!is_closed_ && buffer_.empty()) {
      takeable_.wait(lock);
    }
    if (is_closed_ && buffer_.empty()) {
      return std::nullopt;
    }
    auto retval = std::make_optional(std::move(buffer_.front()));
    buffer_.pop_front();
    return retval;
  }

  void Close() {
    CloseImpl(/*clear=*/false);
  }

  void Cancel() {
    CloseImpl(/*clear=*/true);
  }

 private:
  void CloseImpl(bool clear) {
    std::lock_guard lock(mutex_);
    is_closed_ = true;
    if (clear) {
      buffer_.clear();
    }
    takeable_.notify_all();
  }

 private:
  std::deque<T> buffer_;   // GUARDED BY mutex_
  bool is_closed_{false};  // GUARDED BY mutex_
  twist::stdlike::mutex mutex_;
  twist::stdlike::condition_variable takeable_;
};

}  // namespace tp
