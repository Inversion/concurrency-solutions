#pragma once

#include "semaphore.hpp"

#include <deque>

namespace solutions {

// Bounded Blocking Multi-Producer/Multi-Consumer (MPMC) Queue

template <typename T>
class BlockingQueue {
 public:
  explicit BlockingQueue(size_t capacity)
      : mutex_(1), free_space_(capacity), elements_(0) {
  }

  // Inserts the specified element into this queue
  void Put(T value) {
    free_space_.Acquire();
    mutex_.Acquire();
    buffer_.push_back(std::move(value));
    mutex_.Release();
    elements_.Release();
  }

  // Retrieves and removes the head of this queue,
  // waiting if necessary until an element becomes available
  T Take() {
    elements_.Acquire();
    mutex_.Acquire();
    T retval = std::move(buffer_.front());
    buffer_.pop_front();
    mutex_.Release();
    free_space_.Release();
    return retval;
  }

 private:
  solutions::Semaphore mutex_;
  solutions::Semaphore free_space_;
  solutions::Semaphore elements_;

  std::deque<T> buffer_;
};

}  // namespace solutions
