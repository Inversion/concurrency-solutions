#pragma once

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>

// std::lock_guard, std::unique_lock
#include <mutex>
#include <cstdint>

namespace solutions {

// CyclicBarrier allows a set of threads to all wait for each other
// to reach a common barrier point

// The barrier is called cyclic because
// it can be re-used after the waiting threads are released.

class CyclicBarrier {
 public:
  explicit CyclicBarrier(size_t participants)
      : participants_(participants), arrived_(0), round_(0) {
  }

  // Blocks until all participants have invoked Arrive()
  void Arrive() {
    std::unique_lock lock(mutex_);
    ++arrived_;
    if (arrived_ == participants_) {
      arrived_ = 0, ++round_;
      all_arrived_.notify_all();
    } else {
      const auto round = round_;
      while (round == round_) {
        all_arrived_.wait(lock);
      }
    }
  }

 private:
  const size_t participants_;

  twist::stdlike::condition_variable all_arrived_;
  twist::stdlike::mutex mutex_;
  size_t arrived_;  // GUARDED BY mutex
  size_t round_;    // GUARDED BY mutex
};

}  // namespace solutions
