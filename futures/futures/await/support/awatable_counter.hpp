#pragma once

#include <twist/stdlike/atomic.hpp>
#include <wheels/support/function.hpp>

namespace await::support {
class AwaitableCounter {
 public:
  AwaitableCounter() = default;

  AwaitableCounter(AwaitableCounter&) = delete;
  AwaitableCounter& operator=(const AwaitableCounter&) = delete;

  void IncrementOptimistic(wheels::UniqueFunction<bool()> predicate) {
    counter_.fetch_add(1);
    if (!predicate()) {
      counter_.fetch_sub(1);
    }
  }

  void Decrement() {
    if (counter_.fetch_sub(1) == 1 && (is_waiting_.load() != 0u)) {
      counter_.notify_all();
    }
  }

  void Wait() {
    is_waiting_.store(1);
    while (auto number = counter_.load()) {
      counter_.wait(number);
    }
  }

  bool IsWaiting() {
    return is_waiting_.load() != 0u;
  }

 private:
  twist::stdlike::atomic<uint32_t> counter_{0};
  twist::stdlike::atomic<uint32_t> is_waiting_{0};
};
}  // namespace await::support
