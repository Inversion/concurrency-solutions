#pragma once

#include <await/futures/core/callback.hpp>

#include <await/executors/executor.hpp>
#include <await/executors/helpers.hpp>
#include <await/executors/inline.hpp>

#include <wheels/support/function.hpp>
#include <wheels/support/result.hpp>

#include <twist/stdlike/atomic.hpp>

#include <optional>

namespace await::futures {

namespace detail {

//////////////////////////////////////////////////////////////////////

// State shared between Promise and Future

template <typename T>
class SharedState {
 public:
  SharedState() : executor_{executors::GetInlineExecutor()} {
  }

  void SetResult(wheels::Result<T>&& result) {
    result_.emplace(std::move(result));

    auto state = state_.load();
    for (;;) {
      switch (state) {
        case Empty:
          if (state_.compare_exchange_weak(state, OnlyResult)) {
            return;
          }
          break;
        case OnlyCallback:
          if (state_.compare_exchange_weak(state, Fulfilled)) {
            ExecuteCallback();
            return;
          }
          break;
        case Fulfilled:
        default:
          WHEELS_PANIC("Unexpected state");
      }
    }
  }

  bool HasResult() const {
    return state_.load() == OnlyResult;
  }

  // Precondition: f.IsReady() == true
  wheels::Result<T> GetReadyResult() {
    return std::move(*result_);
  }

  void SetExecutor(executors::IExecutorPtr executor) {
    executor_ = std::move(executor);
  }

  executors::IExecutorPtr GetExecutor() {
    return executor_;
  }

  void SetCallback(Callback<T> callback) {
    callback_.emplace(std::move(callback));

    auto state = state_.load();
    for (;;) {
      switch (state) {
        case Empty:
          if (state_.compare_exchange_weak(state, OnlyCallback)) {
            return;
          }
          break;
        case OnlyResult:
          if (state_.compare_exchange_weak(state, Fulfilled)) {
            ExecuteCallback();
            return;
          }
          break;
        case Fulfilled:
        default:
          WHEELS_PANIC("Unexpected state");
      }
    }
  }

 private:
  void ExecuteCallback() {
    auto task = [result = std::move(*result_),
                 callback = std::move(*callback_)]() mutable {
      callback(std::move(result));
    };

    WHEELS_VERIFY(executor_, "Invalid executor");
    executor_->Execute(std::move(task));
  }

  enum State {
    Empty = 1,
    OnlyResult = 1 << 1,
    OnlyCallback = 1 << 2,
    Fulfilled = 1 << 3
  };

  twist::stdlike::atomic<State> state_{Empty};

  std::optional<wheels::Result<T>> result_;
  std::optional<Callback<T>> callback_;

  executors::IExecutorPtr executor_;
};

//////////////////////////////////////////////////////////////////////

template <typename T>
using StateRef = std::shared_ptr<SharedState<T>>;

template <typename T>
inline StateRef<T> MakeSharedState() {
  return std::make_shared<SharedState<T>>();
}

//////////////////////////////////////////////////////////////////////

// Common base for Promise and Future

template <typename T>
class HoldState {
 protected:
  HoldState(StateRef<T> state) : state_(std::move(state)) {
  }

  // Movable
  HoldState(HoldState&& that) = default;
  HoldState& operator=(HoldState&& that) = default;

  // Non-copyable
  HoldState(const HoldState& that) = delete;
  HoldState& operator=(const HoldState& that) = delete;

  StateRef<T> ReleaseState() {
    CheckState();
    return std::move(state_);
  }

  bool HasState() const {
    return (bool)state_;
  }

  void CheckState() const {
    WHEELS_VERIFY(HasState(), "No shared state or shared state released");
  }

 protected:
  StateRef<T> state_;
};

}  // namespace detail

}  // namespace await::futures
