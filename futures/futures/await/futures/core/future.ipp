#ifndef FUTURE_IMPL
#error Do not include this file directly
#endif

#include <await/futures/core/promise.hpp>

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>

#include <mutex>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

// Static constructors

template <typename T>
Future<T> Future<T>::Completed(T value) {
  auto state = detail::MakeSharedState<T>();
  state->SetResult(wheels::make_result::Ok(std::move(value)));
  return Future<T>(std::move(state));
}

template <typename T>
Future<T> Future<T>::Failed(wheels::Error error) {
  auto state = detail::MakeSharedState<T>();
  state->SetResult(wheels::make_result::Fail(error));
  return Future<T>(std::move(state));
}

template <typename T>
Future<T> Future<T>::Invalid() {
  return Future<T>(nullptr);
}

//////////////////////////////////////////////////////////////////////

// GetResult / GetValue

template <typename T>
wheels::Result<T> Future<T>::GetResult() && {
  twist::stdlike::atomic<uint32_t> is_set{0};
  std::optional<wheels::Result<T>> retval;

  std::move(*this).Subscribe([&is_set, &retval](wheels::Result<T> result) {
    retval.emplace(std::move(result));
    is_set.store(1);
    is_set.notify_one();
  });

  while (is_set.load() != 1) {
    is_set.wait(0);
  }

  return *retval;
}

template <typename T>
T Future<T>::GetValue() && {
  return std::move(*this).GetResult().ValueOrThrow();
}

//////////////////////////////////////////////////////////////////////

// Executors

template <typename T>
Future<T> Future<T>::Via(executors::IExecutorPtr executor) && {
  state_->SetExecutor(std::move(executor));
  return std::move(*this);
}

template <typename T>
executors::IExecutorPtr Future<T>::GetExecutor() const {
  return state_->GetExecutor();
}

template <typename T>
void Future<T>::Subscribe(Callback callback) && {
  ReleaseState()->SetCallback(std::move(callback));
}

//////////////////////////////////////////////////////////////////////

// Transform

template <typename T>
template <typename F>
auto Future<T>::Transform(F&& f) && {
  using U = decltype(f(std::declval<wheels::Result<T>>()));

  wheels::UniqueFunction<U(wheels::Result<T>)> wrapper(std::forward<F>(f));
  // Dispatch
  return std::move(*this).Transform(std::move(wrapper));
}

template <typename T>
template <typename U>
Future<U> Future<T>::Transform(Transformer<U> t) && {
  auto executor = GetExecutor();
  auto [f, p] = MakeContract<U>();

  std::move(*this).Subscribe(
      [p = std::move(p), t = std::move(t)](wheels::Result<T> result) mutable {
        std::move(p).Set(t(std::move(result)));
      });

  return std::move(f).Via(std::move(executor));
}

template <typename T>
template <typename U>
Future<U> Future<T>::Transform(AsyncTransformer<U> t) && {
  auto executor = GetExecutor();
  auto [f, p] = MakeContract<U>();

  auto callback = [p = std::move(p)](wheels::Result<U> result) mutable {
    std::move(p).Set(std::move(result));
  };

  std::move(*this).Subscribe([t = std::move(t), callback = std::move(callback)](
                                 wheels::Result<T> result) mutable {
    t(std::move(result)).Subscribe(std::move(callback));
  });

  return std::move(f).Via(executor);
}

//////////////////////////////////////////////////////////////////////

// Then

template <typename T>
template <typename F>
auto Future<T>::Then(F&& c) && {
  using U = decltype(c(std::declval<T>()));

  wheels::UniqueFunction<U(T)> wrapper(std::forward<F>(c));
  // Dispatch
  return std::move(*this).Then(std::move(wrapper));
}

// Synchronous Then

template <typename T>
template <typename U>
Future<U> Future<T>::Then(Continuation<U> cont) && {
  auto transformer = [cont = std::move(cont)](
                         wheels::Result<T> input) mutable -> wheels::Result<U> {
    if (!input.IsOk()) {
      // Propagate error
      return wheels::make_result::PropagateError(input);
    } else {
      // Invoke continuation
      return wheels::make_result::Invoke(cont, std::move(input));
    }
  };

  return std::move(*this).Transform(std::move(transformer));
}

// Asynchronous Then

template <typename T>
template <typename U>
Future<U> Future<T>::Then(AsyncContinuation<U> cont) && {
  auto transformer =
      [cont = std::move(cont)](wheels::Result<T> input) mutable -> Future<U> {
    if (!input.IsOk()) {
      // Propagate error
      return Future<U>::Failed(input.GetError());
    } else {
      // Invoke continuation
      try {
        return cont(std::move(*input));
      } catch (...) {
        return Future<U>::Failed(std::current_exception());
      }
    }
  };

  return std::move(*this).Transform(std::move(transformer));
}

//////////////////////////////////////////////////////////////////////

// Recover

template <typename T>
Future<T> Future<T>::Recover(ErrorHandler handler) && {
  Transformer<T> transformer =
      [handler = std::move(handler)](
          wheels::Result<T> input) mutable -> wheels::Result<T> {
    if (input.IsOk()) {
      return input;  // Propagate value
    } else {
      // Invoke error handler
      return wheels::make_result::Invoke(handler, input.GetError());
    }
  };

  return std::move(*this).Transform(std::move(transformer));
}

}  // namespace await::futures
