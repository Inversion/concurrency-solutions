#pragma once

#include <await/executors/executor.hpp>
#include <await/futures/core/promise.hpp>

#include <wheels/support/result.hpp>

namespace await::futures {

// Execute callable object `target` via executor `e`
// and return future
//
// Usage:
// auto tp = MakeStaticThreadPool(4, "tp");
// auto future = AsyncVia(tp, []() { return 42; });;

template <typename F>
auto AsyncVia(executors::IExecutorPtr executor, F&& target) {
  using T = decltype(target());

  auto [f, p] = MakeContract<T>();

  executor->Execute([p = std::move(p), target = std::move(target)]() mutable {
    std::move(p).Set(wheels::make_result::Invoke(target));
  });

  return std::move(f).Via(executor);
}

}  // namespace await::futures
