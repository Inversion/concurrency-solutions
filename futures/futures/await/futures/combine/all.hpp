#pragma once

#include <await/futures/core/promise.hpp>
#include <await/futures/combine/detail/combine.hpp>
#include <await/support/non_blocking_queue.hpp>

#include <wheels/support/vector.hpp>
#include <twist/stdlike/atomic.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

// All

namespace detail {

template <typename T>
class AllCombinator {
 public:
  AllCombinator(size_t n) : size_(n) {
  }

  void Combine(wheels::Result<T> result) {
    if (result.HasError()) {
      if (!is_failed_.exchange(true)) {
        std::move(promise_).Set(wheels::make_result::PropagateError(result));
      }
    }

    if (is_failed_.load()) {
      return;
    }

    queue_.Put(std::move(result.ValueUnsafe()));

    if (done_.fetch_add(1) + 1 == size_) {
      auto values = queue_.TakeAll();
      auto result = wheels::make_result::Ok(std::move(values));
      std::move(promise_).Set(std::move(result));
    }
  }

  Future<std::vector<T>> MakeFuture() {
    auto f = promise_.MakeFuture();

    if (size_ == 0) {
      std::vector<T> result{};
      std::move(promise_).Set(wheels::make_result::Ok(std::move(result)));
    }

    return std::move(f);
  }

 private:
  const size_t size_;

  Promise<std::vector<T>> promise_;

  twist::stdlike::atomic<bool> is_failed_{false};
  twist::stdlike::atomic<size_t> done_{0};

  await::support::NonBlockingQueue<T> queue_;
};

}  // namespace detail

// std::vector<Future<T>> -> Future<std::vector<T>>
// All values | first error

template <typename T>
Future<std::vector<T>> All(std::vector<Future<T>> inputs) {
  return Combine<detail::AllCombinator, T>(std::move(inputs));
}

template <typename T, typename... Fs>
Future<std::vector<T>> All(Future<T>&& first, Fs&&... rest) {
  return All(wheels::ToVector(std::move(first), std::forward<Fs>(rest)...));
}

}  // namespace await::futures
