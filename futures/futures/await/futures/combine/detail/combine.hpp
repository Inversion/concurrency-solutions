#pragma once

#include <await/futures/core/future.hpp>

#include <wheels/support/result.hpp>

#include <vector>
#include <memory>

namespace await::futures::detail {

// Generic algorithm

template <template <typename> class Combinator, typename T>
auto Combine(std::vector<Future<T>> inputs) {
  auto combinator = std::make_shared<Combinator<T>>(inputs.size());

  for (auto& f : inputs) {
    std::move(f).Subscribe([combinator](wheels::Result<T> result) mutable {
      combinator->Combine(result);
    });
  }

  return combinator->MakeFuture();
}

}  // namespace await::futures::detail
