#pragma once

#include <await/futures/core/promise.hpp>
#include <await/futures/combine/detail/combine.hpp>
#include <await/support/non_blocking_queue.hpp>

#include <wheels/support/vector.hpp>
#include <twist/stdlike/atomic.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

// FirstOf

namespace detail {

template <typename T>
class FirstOfCombinator {
 public:
  FirstOfCombinator(size_t n) : size_(n) {
  }

  void Combine(wheels::Result<T> result) {
    if (result.IsOk()) {
      if (is_done_.exchange(1) == 0) {
        std::move(promise_).Set(std::move(result));
      }
    } else {
      if (failed_.fetch_add(1) + 1 == size_) {
        std::move(promise_).Set(std::move(result));
      }
    }
  }

  Future<T> MakeFuture() {
    if (size_ == 0) {
      return Future<T>::Invalid();
    }

    return promise_.MakeFuture();
  }

 private:
  const size_t size_;

  twist::stdlike::atomic<uint32_t> is_done_{0};
  twist::stdlike::atomic<uint32_t> failed_{0};

  Promise<T> promise_;
};

}  // namespace detail

// std::vector<Future<T>> -> Future<T>
// First value | last error
// Returns invalid future on empty input

template <typename T>
Future<T> FirstOf(std::vector<Future<T>> inputs) {
  return Combine<detail::FirstOfCombinator, T>(std::move(inputs));
}

template <typename T, typename... Fs>
auto FirstOf(Future<T>&& first, Fs&&... rest) {
  return FirstOf(wheels::ToVector(std::move(first), std::forward<Fs>(rest)...));
}

}  // namespace await::futures
