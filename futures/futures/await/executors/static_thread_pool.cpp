#include <await/executors/static_thread_pool.hpp>

#include <await/executors/label_thread.hpp>
#include <await/executors/helpers.hpp>

#include <await/support/awatable_counter.hpp>
#include <await/support/blocking_queue.hpp>

#include <twist/stdlike/atomic.hpp>

#include <thread>
#include <vector>

namespace await::executors {

class StaticThreadPool : public IThreadPool {
  void StartWorkers(size_t threads, const std::string& name);

  void CloseProperly();

 public:
  StaticThreadPool(size_t threads, const std::string& name);

  StaticThreadPool(StaticThreadPool&) = delete;
  StaticThreadPool& operator=(const StaticThreadPool&) = delete;

  void Execute(Task&& task) override;

  void Join() override;

  void Shutdown() override;

  size_t ExecutedTaskCount() const override;

  ~StaticThreadPool() override;

 private:
  twist::stdlike::atomic<size_t> tasks_executed_;
  std::vector<std::thread> workers_;

  support::BlockingQueue<Task> tasks_;
  support::AwaitableCounter joiner_;

  bool is_closed_properly_{false};
};

StaticThreadPool::StaticThreadPool(size_t threads, const std::string& name) {
  StartWorkers(threads, name);
}

void StaticThreadPool::StartWorkers(size_t threads, const std::string& name) {
  auto routine = [name = name, &tasks = tasks_, &joiner = joiner_,
                  &tasks_executed = tasks_executed_]() {
    LabelThread(name);
    while (auto task = tasks.Get()) {
      ExecuteHere(*task);
      joiner.Decrement();
      tasks_executed.fetch_add(1);
    }
  };

  for (size_t i = 0; i < threads; ++i) {
    workers_.emplace_back(routine);
  }
}

void StaticThreadPool::Execute(Task&& task) {
  joiner_.IncrementOptimistic([&task, &tasks = tasks_] {
    return tasks.Put(std::move(task));
  });
}

void StaticThreadPool::Join() {
  joiner_.Wait();
  tasks_.Close();
  CloseProperly();
}

void StaticThreadPool::Shutdown() {
  tasks_.Cancel();
  CloseProperly();
}

size_t StaticThreadPool::ExecutedTaskCount() const {
  return tasks_executed_.load();
}

StaticThreadPool::~StaticThreadPool() {
  if (!is_closed_properly_) {
    Shutdown();
  }
}

void StaticThreadPool::CloseProperly() {
  for (auto& worker : workers_) {
    worker.join();
  }

  is_closed_properly_ = true;
}

IThreadPoolPtr MakeStaticThreadPool(size_t threads, const std::string& name) {
  return std::make_shared<StaticThreadPool>(threads, name);
}

}  // namespace await::executors
