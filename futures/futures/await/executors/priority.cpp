#include <await/executors/priority.hpp>

#include <await/executors/helpers.hpp>

#include <await/support/priority_queue.hpp>

#include <queue>
#include <memory>

namespace await::executors {

class PriorityExecutor : public IPriorityExecutor,
                         public std::enable_shared_from_this<PriorityExecutor> {
 public:
  PriorityExecutor(IExecutorPtr executor);

  void Execute(int priority, Task&& task) override;

  IExecutorPtr FixPriority(int priority) override;

  ~PriorityExecutor() override = default;

 private:
  support::PriorityQueue<Task> queue_;
  IExecutorPtr executor_;
};

class FixedPriorityExecutor : public IExecutor {
 public:
  FixedPriorityExecutor(int priority, IPriorityExecutorPtr executor);

  void Execute(Task&& task) override;

  ~FixedPriorityExecutor() override = default;

 private:
  int priority_;
  IPriorityExecutorPtr executor_;
};

PriorityExecutor::PriorityExecutor(IExecutorPtr executor)
    : executor_(executor) {
}

void PriorityExecutor::Execute(int priority, Task&& task) {
  queue_.Put(priority, std::move(task));

  auto this_ptr = shared_from_this();
  executor_->Execute([this_ptr] {
    auto task = this_ptr->queue_.Get();
    ExecuteHere(task);
  });
}

IPriorityExecutorPtr MakePriorityExecutor(IExecutorPtr executor) {
  return std::make_shared<PriorityExecutor>(executor);
}

IExecutorPtr PriorityExecutor::FixPriority(int priority) {
  auto this_ptr = shared_from_this();
  return std::make_shared<FixedPriorityExecutor>(priority, this_ptr);
}

FixedPriorityExecutor::FixedPriorityExecutor(int priority,
                                             IPriorityExecutorPtr executor)
    : priority_(priority), executor_(executor) {
}

void FixedPriorityExecutor::Execute(Task&& task) {
  executor_->Execute(priority_, std::move(task));
}
}  // namespace await::executors
