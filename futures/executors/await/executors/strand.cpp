#include <await/executors/strand.hpp>
#include <await/executors/helpers.hpp>

#include <await/support/non_blocking_queue.hpp>

#include <twist/stdlike/atomic.hpp>

#include <memory>

namespace await::executors {

class StrandExecutor : public IExecutor,
                       public std::enable_shared_from_this<StrandExecutor> {
  void Step();
  void Resume();
  void Schedule();

 public:
  StrandExecutor(IExecutorPtr executor);

  void Execute(Task&& task) override;

  ~StrandExecutor() override = default;

 private:
  IExecutorPtr executor_;
  support::NonBlockingQueue<Task> queue_;

  twist::stdlike::atomic<bool> is_working_{false};
};

StrandExecutor::StrandExecutor(IExecutorPtr executor) : executor_(executor) {
}

void StrandExecutor::Execute(Task&& task) {
  queue_.Put(std::move(task));
  Schedule();
}

void StrandExecutor::Step() {
  auto tasks = queue_.TakeBatch();
  for (auto& task : tasks) {
    ExecuteHere(task);
  }
  is_working_.store(false);
}

void StrandExecutor::Resume() {
  if (!queue_.IsEmpty()) {
    Schedule();
  }
}

void StrandExecutor::Schedule() {
  if (!is_working_.exchange(true)) {
    auto this_ptr = shared_from_this();
    executor_->Execute([this_ptr] {
      this_ptr->Step();
      this_ptr->Resume();
    });
  }
}

IExecutorPtr MakeStrand(IExecutorPtr executor) {
  return std::make_shared<StrandExecutor>(executor);
}

}  // namespace await::executors
