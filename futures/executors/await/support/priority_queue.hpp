#pragma once

#include <twist/stdlike/mutex.hpp>

#include <utility>
#include <vector>
#include <queue>

namespace await::support {
template <typename T>
class PriorityQueue {
  using ElemT = std::pair<int, T>;

  const struct {
    bool operator()(const ElemT& lhs, const ElemT& rhs) const {
      return lhs.first <= rhs.first;
    }
  } priority_{};

 public:
  PriorityQueue() = default;

  PriorityQueue(PriorityQueue&) = delete;
  PriorityQueue& operator=(const PriorityQueue&) = delete;

  void Put(int priority, T value) {
    std::unique_lock lock(mutex_);
    queue_.emplace_back(priority, std::move(value));
    std::push_heap(queue_.begin(), queue_.end(), priority_);
  }

  T Get() {
    std::unique_lock lock(mutex_);
    std::pop_heap(queue_.begin(), queue_.end(), priority_);
    auto retval = std::move(queue_.back().second);
    queue_.pop_back();
    return retval;
  }

 private:
  std::vector<ElemT> queue_;  // GUARDED BY mutex_
  twist::stdlike::mutex mutex_;
};
}  // namespace await::support
