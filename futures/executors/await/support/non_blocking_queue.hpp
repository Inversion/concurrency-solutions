#pragma once

#include <twist/stdlike/atomic.hpp>

#include <list>
#include <optional>
#include <list>

namespace await::support {
// MULTI PRODUCERS SINGLE CONSUMER LOCK FREE QUEUE!!111!!!11
template <typename T>
class NonBlockingQueue {
  struct Node {
    T value;
    Node* next;

    Node(T value, Node* next) : value(std::move(value)), next(next) {
    }
  };

 public:
  NonBlockingQueue() {
  }

  void Put(T value) {
    auto ins = new Node(std::move(value), head_.load());

    while (!head_.compare_exchange_weak(ins->next, ins)) {
    }
  }

  std::list<T> TakeBatch() {
    std::list<T> retval;
    for (auto current = head_.exchange(nullptr); current;) {
      retval.emplace_front(std::move(current->value));
      auto temp = current;
      current = current->next;
      delete temp;
    }

    return retval;
  }

  bool IsEmpty() const {
    return head_.load() == nullptr;
  }

  ~NonBlockingQueue() {
    TakeBatch();
  }

 private:
  twist::stdlike::atomic<Node*> head_{nullptr};
};
}  // namespace await::support
