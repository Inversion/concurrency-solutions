#pragma once

#include <twist/stdlike/mutex.hpp>
#include <twist/stdlike/condition_variable.hpp>

#include <deque>
#include <optional>

namespace await::support {
template <typename T>
class BlockingQueue {
 public:
  BlockingQueue() = default;
  BlockingQueue(BlockingQueue&) = delete;
  BlockingQueue& operator=(const BlockingQueue&) = delete;

  bool Put(T value) {
    std::unique_lock lock(mutex_);
    if (is_closed_) {
      return false;
    }
    container_.emplace_back(std::move(value));
    takeable_.notify_one();
    return true;
  }

  std::optional<T> Get() {
    std::unique_lock lock(mutex_);
    while (!is_closed_ && container_.empty()) {
      takeable_.wait(lock);
    }
    if (is_closed_ && container_.empty()) {
      return std::nullopt;
    }
    auto retval = std::make_optional(std::move(container_.front()));
    container_.pop_front();
    return retval;
  }

  void Close() {
    CloseImpl(/*clear=*/false);
  }

  void Cancel() {
    CloseImpl(/*clear=*/true);
  }

 private:
  void CloseImpl(bool clear) {
    std::unique_lock lock(mutex_);
    is_closed_ = true;
    if (clear) {
      container_.clear();
    }
    takeable_.notify_all();
  }

 private:
  std::deque<T> container_;  // GUARDED BY mutex_
  bool is_closed_{false};    // GUARDED BY mutex_
  twist::stdlike::mutex mutex_;

  twist::stdlike::condition_variable takeable_;
};
}  // namespace await::support
