#pragma once

#include <twist/stdlike/atomic.hpp>
#include <twist/util/spin_wait.hpp>

#include <cstdlib>

namespace solutions {

using twist::util::SpinWait;

class Mutex {
 public:
  void Lock() {
    waiting_.fetch_add(1);
    while (acquired_.exchange(1) != 0u) {
      acquired_.wait(1);
    }
  }

  void Unlock() {
    acquired_.store(0);
    if (waiting_.fetch_sub(1) != 0u) {
      acquired_.notify_one();
    }
  }

 private:
  twist::stdlike::atomic<unsigned int> acquired_{0};
  twist::stdlike::atomic<unsigned int> waiting_{0};
};

}  // namespace solutions
